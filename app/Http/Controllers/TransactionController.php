<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller {

    public function index() {
        $transactions = Transaction::all();

        return response()->json([
                    'success' => true,
                    'data' => $transactions
        ]);
    }

    public function show($id) {
        $transaction = Transaction::find($id);

        if (!$transaction) {
            return response()->json([
                        'success' => false,
                        'message' => 'Transaction with id ' . $id . ' not found'
                            ], 400);
        }

        return response()->json([
                    'success' => true,
                    'data' => $transaction->toArray()
                        ], 400);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'transactioncode' => 'required',
            'amount' => 'required|integer',
            'account_no' => 'required',
            'phone_no' => 'required'
        ]);

        $mpesa_transaction = new Transaction();
        $mpesa_transaction->transactioncode = $request->transactioncode;
        $mpesa_transaction->amount = $request->amount;
        $mpesa_transaction->account_no = $request->account_no;
        $mpesa_transaction->phone_no = $request->phone_no;

        if ($mpesa_transaction->save())
            return response()->json([
                        'success' => true,
                        'data' => $mpesa_transaction->toArray()
            ]);
        else
            return response()->json([
                        'success' => false,
                        'message' => 'Transaction could not be added'
                            ], 500);
    }

    public function update(Request $request, $id) {
        $transaction = Transaction::find($id);

        if (!$transaction) {
            return response()->json([
                        'success' => false,
                        'message' => 'Transaction with id ' . $id . ' not found'
                            ], 400);
        }

        $updated = $transaction->fill($request->all())->save();

        if ($updated)
            return response()->json([
                        'success' => true
            ]);
        else
            return response()->json([
                        'success' => false,
                        'message' => 'Transaction could not be updated'
                            ], 500);
    }

    public function destroy($id) {
        $transaction = Transaction::find($id);

        if (!$transaction) {
            return response()->json([
                        'success' => false,
                        'message' => 'Transaction with id ' . $id . ' not found'
                            ], 400);
        }

        if ($transaction->delete()) {
            return response()->json([
                        'success' => true
            ]);
        } else {
            return response()->json([
                        'success' => false,
                        'message' => 'Transaction could not be deleted'
                            ], 500);
        }
    }

    public function validated_transactions() {
        $filter_param = ['validated' => '0', 'transmitted' => '0'];
        $transaction = Transaction::where($filter_param)->first();

        if (!$transaction) {
            return response()->json([
                        'success' => false,
                        'message' => 'No invalidated Transactions were  found in the platform '
                            ], 400);
        } else {

            foreach ($transaction as $key => $value) {
                $account_no = '';
                $phone_no = '';
                $transaction_code = '';
                $amount = '';
                if ($key == 'account_no') {
                    $account_no = $value;
                }

                if ($key == 'phone_no') {
                    $phone_no = $value;
                }

                if ($key == 'transaction_code') {
                    $transaction_code = $value;
                }


                if ($key == 'amount') {
                    $amount = $value;
                }

                try {

                    $url = 'https://financesystem.com/validate_transaction';
                    $data = array('account_no' => $account_no, 'phone_no' => $phone_no, 'transaction_code' => $transaction_code, 'amount' => $amount);

                    // use key 'http' even if you send the request to https://...
                    $options = array(
                        'http' => array(
                            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                            'method' => 'POST',
                            'content' => http_build_query($data)
                        )
                    );
                    $context = stream_context_create($options);
                    $result = file_get_contents($url, false, $context);
                    if ($result === FALSE) { /* Handle error */
                        return response()->json([
                                    'success' => false,
                                    'message' => "Internal server error "
                                        ], 500);
                    }

                    return response()->json([
                                'success' => true,
                                'message' => $result
                                    ], 200);
                } catch (Exception $exc) {
                    $message = $exc->getTraceAsString();

                    Log::error($message);

                    return response()->json([
                                'success' => false,
                                'message' => "Internal server error "
                                    ], 500);
                }
            }
        }
    }

}
