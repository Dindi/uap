<?php
/*

 * Loads the  accountants user interface to view and display
 *  transactional data from mpesa transactions
 *  */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\Transaction;

class AccountCotroller extends Controller {
    

    /**
     * Display a listing of the transactions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return DataTables::of(Transaction::query())->make(true);
    }

    /**
     * Show the form for creating a new transaction (not yet full implemented) .
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('transactions');
    }

}
