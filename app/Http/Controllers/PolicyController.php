<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PolicyController extends Controller {
    //

    /**
     * Handles Get Policy Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_policy_details(Request $request) {

        $request_value = $request->value;
        $request_type = $request->request_type;
        $customer_id = "";
        $policy_number = "";
        $full_names = "";

        if ($request_type === 'JSON') {




            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_PORT => "3000",
                CURLOPT_URL => "http://policysystem.com/get_policy_details",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"policy_number\"\r\n\r\n$request_value\r\n",
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json",
                    "Authorization:  eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgyMzNlMjBhNTEyNTk1ZjQyNGRlN2E2YWM3ZWJlYjFmMWM3ODRiM2ZlYzY2NGUyZjdhMjNiMGRjNDM1ZTllOGExMWM3OTg2NDUyNjhhNzFiIn0.eyJhdWQiOiIxIiwianRpIjoiODIzM2UyMGE1MTI1OTVmNDI0ZGU3YTZhYzdlYmViMWYxYzc4NGIzZmVjNjY0ZTJmN2EyM2IwZGM0MzVlOWU4YTExYzc5ODY0NTI2OGE3MWIiLCJpYXQiOjE1NTM2NDU0MDgsIm5iZiI6MTU1MzY0NTQwOCwiZXhwIjoxNTg1MjY3ODA4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.ZbgAwNx_Y1nISXw9lK6RkLduocscx0HwovOzFoihj70oonUPUZ8ShQSitcAB9mrljHL6iNjH6ZCcDopbSFZH4fkC7-2UMoos2LX3HPYyq_ZiDpQH9ntzgn6-wo3mRQtbDXmcdZeXrNDZbidcTR9DAdjjw5dvGmwpsnk5MjguXq9ZPCAcbdIeWN2ONgn19ra0TeXadroQbdWyu1SJKgaW3_DSU6oEVooPHb4C6uBEGR2NHe7OpnLimUAeF0SjO7TXrltJLYT-wtikmDUIexXAJtzY6Mi4tpV27jTDDkEmw3XyR6RfZBbdA4Xy40LLVgTpuNj-hy9p7YLPuf_XEZi6-gVZUTgf3qgP6arvpGpfAwZVGbUcKrorJcCgYu75ty6ZdIC0bRwrV4EEg1aOqSZGEADzESjNRz0X89GI6ywTWeDLx-C6XMNoA0gSL0fB_g9GMW5A5fb9FBk7bQX4a3qW2htC3boGtWxtseoaQTLkbUz9pbAmTXKjD1VbRwigNAXUy_BLpDpA_tGi7ElylAZEBPiQLFZGw7PqK-ij_amjV8GJvwCAPGBpVvaUU1IL_fVhLDpFX51C1jbLB0H38pnb1zyyl1QmxNGuoEgpPM5Dd47kw_qv0QlinyyZH6GVGWc1fLJh4pVZLWnAXnuQL8YFiET8F9rP7KeK6EjTG1ycNZo",
                    "cache-control: no-cache",
                    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {

                $obj_response = json_decode($response, TRUE);

                foreach ($obj_response as $key => $value) {
                    if ($key == 'CUSTOMER_ID') {
                        $customer_id = $value;
                    }

                    if ($key == 'POLICY_NUMBER') {
                        $policy_number = $value;
                    }

                    if ($key == 'FULL_NAMES') {
                        $full_names = $value;
                    }
                }
            }
        } else if ($request_type === 'SOAP') {


            ini_set('soap.wsdl_cache_enabled', 0);
            ini_set('soap.wsdl_cache_ttl', 900);
            ini_set('default_socket_timeout', 15);


            $params = array('policy_number' => $request_value);


            $wsdl = 'http://policysystem.com/get_policy_details?WSDL';

            $options = array(
                'uri' => 'http://schemas.xmlsoap.org/soap/envelope/',
                'style' => SOAP_RPC,
                'use' => SOAP_ENCODED,
                'soap_version' => SOAP_1_1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'connection_timeout' => 15,
                'trace' => true,
                'encoding' => 'UTF-8',
                'exceptions' => true,
            );
            try {
                $soap = new SoapClient($wsdl, $options);
                $data = $soap->method($params);
            } catch (Exception $e) {
                die($e->getMessage());
            }

            var_dump($data);

            $cleaned_data = array();
            foreach ($data->MPESA_Transaction->transaction as $item) {
                $cleaned_data[] = get_object_vars($item);

                $customer_id = $item->CUSTOMER_ID;
                $policy_number = $item->POLICY_NUMBER;
                $full_names = $item->FULL_NAMES;
            }

            die;
        }
    }

}
