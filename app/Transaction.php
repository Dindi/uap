<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    //
    protected $fillable = [
        'transactioncode', 'amount','account_no','phone_no','id'
    ];

}
