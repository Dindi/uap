@extends('layouts.app')

@section('content')


<!-- Our body starts from here  -->
<div class="container">
    <h2>Accountants Transactions report</h2>
    <table class="table table-bordered" id="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Transaction code</th>
                <th>Amount </th>
                <th>Account no</th>
                <th>Phone no </th>
            </tr>
        </thead>
    </table>
</div>
<!-- script tag starts here -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">  
<link  href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>  
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
    var j = jQuery.noConflict();
    j(document).ready(function () {


        j(function () {
            /*
             * initialize and load the datatables plugin
             * using the url , data is loaded from the  browser
             *   */
            j('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('index') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'transactioncode', name: 'transactioncode'},
                    {data: 'amount', name: 'amount'},
                    {data: 'account_no', name: 'account_no'},
                    {data: 'phone_no', name: 'phone_no'}
                ]
            });
        });
    });
</script>
<!-- script tag ends here -->
<!-- our body ends from here -->


@endsection