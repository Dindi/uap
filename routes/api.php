<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */




Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');

    Route::get('mpesa_transactions', 'TransactionController@index');
    Route::get('transaction/{id}', 'TransactionController@show');
    Route::post('new_transaction', 'TransactionController@store');
    Route::post('update_transaction/{id}', 'TransactionController@update');
    Route::get('valid_transactions', 'TransactionController@validated_transactions');

    Route ::get('GetCustomerPolicyBasicDetails', 'PolicyController@get_policy_details');

    Route::post('finance_post', 'TransactionController@validated_transactions');
});


