<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('home');
});

Route::middleware('auth')->group(function () {
    Route::get('create', 'AccountCotroller@create');
    Route::get('index', 'AccountCotroller@index');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
